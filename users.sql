-- Adminer 4.7.7 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP DATABASE IF EXISTS `lukas97811`;
CREATE DATABASE `lukas97811` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `lukas97811`;

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `jmeno` text CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL,
  `vek` text CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL,
  `vaha` text CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL,
  `barva pleti` text CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL,
  `rasa` text CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL,
  `mzda` text CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL,
  `vyska` text CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL,
  `povolani` text CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL,
  `datum narozeni` datetime NOT NULL,
  `pohlavi` varchar(255) CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `users` (`id`, `jmeno`, `vek`, `vaha`, `barva pleti`, `rasa`, `mzda`, `vyska`, `povolani`, `datum narozeni`, `pohlavi`) VALUES
(1,	'Lukáš Trapl',	'17',	'80',	'bílá',	'europoidni',	'25000',	'186',	'arcivevoda',	'0000-00-00 00:00:00',	'muž'),
(2,	'Jakub Plechšmíd',	'18',	'80',	'bílá',	'europoidní',	'10',	'1234',	'školník',	'0000-00-00 00:00:00',	'nemá'),
(3,	'Michal Musil',	'18',	'160',	'bílá',	'europoidni',	'20000',	'185',	'mekmanažer nebo tak neco',	'0000-00-00 00:00:00',	'muž'),
(4,	'Vlada Luta',	'69',	'69',	'ruzova',	'europoidni',	'69',	'69',	'nemá',	'0000-00-00 00:00:00',	'muž'),
(5,	'Filip Neumann',	'17',	'76',	'nemá',	'europoidni',	'10000',	'185',	'Nik Tendo',	'0000-00-00 00:00:00',	'žena'),
(6,	'Nik Tendo',	'83',	'41',	'bílá',	'europoidni',	'150000',	'275',	'Yzomandias',	'0000-00-00 00:00:00',	'muž'),
(7,	'Jakub Vagner',	'99',	'140',	'bílá',	'europoidni',	'50000',	'1800',	'bitcoin věc',	'0000-00-00 00:00:00',	'nemá'),
(8,	'Milan Lukeš',	'12',	'20',	'modrá',	'europoidni',	'30000',	'130',	'zena v domacnosti',	'0000-00-00 00:00:00',	'žena'),
(9,	'Ondřej Šplíchal',	'1',	'8',	'bílá',	'mongoloidní',	'1',	'64',	'nema',	'0000-00-00 00:00:00',	'muž'),
(10,	'Richard Sipek',	'9',	'50',	'bila',	'europoidni',	'0',	'140',	'nema',	'0000-00-00 00:00:00',	'muž');

-- 2021-02-08 10:41:22
